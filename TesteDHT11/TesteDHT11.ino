#include <DHTesp.h>

#define PINODHT 23
#define TIPODHT DHTesp::DHT11

DHTesp dht;

void setup() {
  Serial.begin(115200);
  dht.setup(PINODHT, TIPODHT);
}

void loop() {
  Serial.print("Umidade: ");
  Serial.print(dht.getHumidity());
  Serial.print(" Temperatura: ");
  Serial.print(dht.getTemperature());
  Serial.println(" °C");
  delay(10000);

}
