bool verificarStatusUltima(String url, String id, String token, bool *logado, bool *atendida) {
	HTTPClient http;
	
	StaticJsonDocument<400> respostaJson;
	DeserializationError erro;
	
	String resposta;
	bool retorno;
	
	http.begin(url + "requisicoes/atendidas/ver/ultima/" + id);
    http.addHeader("Content-Type", "application/json");
	http.addHeader("Authorization", "Bearer " + token);
	int respostaCodigo = http.GET();
	
	if(respostaCodigo > 0) {
		if(respostaCodigo == 401 || respostaCodigo == 403) {
			*logado = false;
			Serial.println("Não autorizado!");
			
			retorno = false;
		}
		else {
			resposta = http.getString();
			erro = deserializeJson(respostaJson, resposta);
		
			if(erro) {
				Serial.print(F("Impossível interpretar Json. Código do erro: "));
				Serial.println(erro.c_str());
		
				retorno = false;
			}
			else {
				*atendida = respostaJson["célulaAtendeu"];
				retorno = true;
			}
		}
	}
	else {
	  Serial.print("Erro ao realizar requisição.: ");
	  Serial.println(http.errorToString(respostaCodigo));
      retorno = false;
	}
	
	http.end();
	return retorno;
	
}