bool logar(String url, String *token) {
    HTTPClient http;
    const int capacidadePedido = JSON_OBJECT_SIZE(2);
    const int capacidadeResposta = JSON_OBJECT_SIZE(100);

    StaticJsonDocument<capacidadePedido> credenciais;
    StaticJsonDocument<capacidadeResposta> respostaJson;
    DeserializationError erro;

    String saida;
    String entrada;
	bool retorno;
    
    credenciais["NomeUsuário"] = "monitor";
    credenciais["Senha"] = "123456";

    serializeJson(credenciais, saida);
	
    http.begin(url + "contas/login/");
    http.addHeader("Content-Type", "application/json");
    int resposta = http.POST(saida);

    if(resposta > 0) {
      entrada = http.getString();
      erro = deserializeJson(respostaJson, entrada);

      if(erro) {
        Serial.print(F("Impossível interpretar Json. Código do erro: "));
        Serial.println(erro.c_str());
		
        retorno = false;
      }
      else {
        Serial.println("Logado com sucesso.");
        String tk = respostaJson["token"];
        *token = tk;
        retorno = true;
      }
    }
    else {
      Serial.print("Erro ao realizar requisição: ");
	  Serial.println(http.errorToString(resposta));
      retorno = false;
    }
	
	http.end();
	return retorno;
}
