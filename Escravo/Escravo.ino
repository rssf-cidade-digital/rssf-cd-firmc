#include <WiFi.h>
#include "./Controladores/Login.h"
#include "./Controladores/Requisicao.h"

#define LED_TOKEN 2
#define TEMPO_DEMORA 10000

//const char* ssid = "Aurivan";
//const char* senha = "03grapalth";

const char* ssid = "Rally 2.4";
const char* senha = "Rally2019";

String url = "http://10.100.1.98:5001/";
String id = "1";

bool logado = false;
String token;

void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, senha);
  
  pinMode(LED_TOKEN, OUTPUT);
  digitalWrite(LED_TOKEN, HIGH);
  
  Serial.print("Conectando à rede.");
  while(WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
    digitalWrite(LED_TOKEN, LOW);
    delay(500);
    digitalWrite(LED_TOKEN, HIGH);
  }

  Serial.println("");
  Serial.print("IP: ");
  Serial.println(WiFi.localIP());
  Serial.print("MAC: ");
  Serial.println(WiFi.macAddress());
}

void loop() {
  if(WiFi.status() == WL_CONNECTED) {
    if(!logado) {
      digitalWrite(LED_TOKEN, HIGH);
      logado = logar(url, &token);
    }
    else {
      digitalWrite(LED_TOKEN, LOW);
      bool atendida;
      if(verificarStatusUltima(url, id, token, &logado, &atendida)) {
        if(atendida) {
          Serial.println("Última requisição atendida.");
        }
        else {
          Serial.println("Última requisição NÃO foi atendida.");
          delay(TEMPO_DEMORA);
        }
      }
    }
  }
  else {
    Serial.println("Conexão perdida!");
    delay(1000);
  }
}
