#include "./Configuracoes.h"
#include <WiFiRSSF.h>
#include <EscravoRSSF.h>

Escravo escravo(HTTPS, SERVIDOR, PORTA_API, ID_CELULA, PINO_LED, AMOSTRAS, TEMPO_ESPERA_MONITORAMENTO);
WiFiRSSF wifi(PINO_LED, SSID_WIFI, SENHA_WIFI);

void setup() 
{
  Serial.begin(115200);
  escravo.configurar();
  escravo.configurarPinosMonitoramento(PINO_DHT, PINO_UV, PINO_FOGO, PINO_TOXICO, PINO_FUMACA);
  wifi.configurar();
  wifi.conectar();
}

void loop() 
{
  if(wifi.conectado()) 
  {
    if(escravo.logado) 
    {
      escravo.verificarUltimaRequisicao();
      if(escravo.requisicaoParaAtender)
      {
        escravo.atenderUltimaRequisicao();
      }
      delay(TEMPO_ESPERA);
    }
    else 
    {
      escravo.logar();
    }
  }
  else
  {
    wifi.conexaoPerdida();
  }
}
